<?php

/**
 * @file
 * File-based caching API
 */

define('FASTPATH_FSCACHE_PATH', './files/cache');

/**
 * Return data from the persistent cache.
 *
 * @param $key
 *   The cache ID of the data to retrieve.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache' for the default cache.
 */function cache_get($key, $table = 'cache') {
  global $user;
  
  $cache_lifetime = variable_get('cache_lifetime', 0);

  if (variable_get('page_cache_fastpath', 0)) {
    $cache = cache_get_file($key, $table);
  }
  else {
    // garbage collection necessary when enforcing a minimum cache lifetime
    $cache_flush = variable_get('cache_flush', 0);
    if ($cache_flush && ($cache_flush + $cache_lifetime <= time())) {
      variable_set('cache_flush', 0);
    }
  }

  if (isset($cache->data)) {
    // If enforcing a minimum cache lifetime, validate that the data is
    // currently valid for this user before we return it by making sure the
    // cache entry was created before the timestamp in the current session's
    // cache timer. The cache variable is loaded into the $user object by
    // sess_read() in session.inc.
    if (is_object($user) && ($user->cache > $cache->created)) {
      // This cache data is too old and thus not valid for us, ignore it.
      return 0;
    }
    return $cache;
  }
  return 0;
}

/**
 * Return data from the persistent disk cache.
 *
 * @param $key
 *   The cache ID of the data to retrieve.
 */
function cache_get_file($key, $table) {
  $cache = NULL;
  $cache_file = cache_filename($key, $table);

  if (file_exists($cache_file)) {    
    if ($fp = fopen($cache_file, 'r')) {
      $filesize = filesize($cache_file);
      if ($filesize > 0 && flock($fp, LOCK_SH)) {
        $data = fread($fp, $filesize);
        flock($fp, LOCK_UN);
        $cache = unserialize($data);
      }
      fclose($fp);
    }
  }
  return $cache;
}


/**
 * Store data in the persistent cache.
 *
 * The persistent cache is split up into four database
 * tables. Contributed modules can add additional tables.
 *
 * 'cache_page': This table stores generated pages for anonymous
 * users. This is the only table affected by the page cache setting on
 * the administrator panel.
 *
 * 'cache_menu': Stores the cachable part of the users' menus.
 *
 * 'cache_filter': Stores filtered pieces of content. This table is
 * periodically cleared of stale entries by cron.
 *
 * 'cache': Generic cache storage table.
 *
 * The reasons for having several tables are as follows:
 *
 * - smaller tables allow for faster selects and inserts
 * - we try to put fast changing cache items and rather static
 *   ones into different tables. The effect is that only the fast
 *   changing tables will need a lot of writes to disk. The more
 *   static tables will also be better cachable with MySQL's query cache
 *
 * @param $cid
 *   The cache ID of the data to store.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache'.
 * @param $data
 *   The data to store in the cache. Complex data types must be serialized first.
 * @param $expire
 *   One of the following values:
 *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
 *     explicitly told to using cache_clear_all() with a cache ID.
 *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
 *     general cache wipe.
 *   - A Unix timestamp: Indicates that the item should be kept at least until
 *     the given time, after which it behaves like CACHE_TEMPORARY.
 * @param $headers
 *   A string containing HTTP header information for cached pages.
 */function cache_set($cid, $table = 'cache', $data, $expire = CACHE_PERMANENT, $headers = NULL) {
  static $subdirectories;
  
  if (variable_get('page_cache_fastpath', 0)) {
    // prepare the cache before grabbing the file lock
    $cache->cid = $cid;
    $cache->table = $table;
    $cache->data = $data;
    $cache->created = time();
    $cache->expire = $expire;
    $cache->headers = $headers;
    $data = serialize($cache);
    
    $file = cache_filename($cid, $table);
    if ($fp = @fopen($file, 'w')) {
      // only write to the cache file if we can obtain an exclusive lock
      if (flock($fp, LOCK_EX)) {
        fwrite($fp, $data);
        flock($fp, LOCK_UN);
      }
      fclose($fp);
    }
    else {
      // t() may not be loaded
      drupal_set_message(strtr('Cache write error, failed to open file "%file"', array('%file' => $file)), 'error');
    }
  }
}

/**
 *
 * Expire data from the cache. If called without arguments, expirable
 * entries will be cleared from the cache_page table.
 *
 * @param $cid
 *   If set, the cache ID to delete. Otherwise, all cache entries that can
 *   expire are deleted.
 *
 * @param $table
 *   If set, the table $table to delete from. Mandatory
 *   argument if $cid is set.
 *
 * @param $wildcard
 *   If set to TRUE, the $cid is treated as a substring
 *   to match rather than a complete ID. The match is a right hand
 *   match. If '*' is given as $cid, the table $table will be emptied.
 */
function cache_clear_all($cid = NULL, $table = NULL, $wildcard = FALSE) {
  global $user;
  
  $file_cache = variable_get('fastpath_fscache_path', FASTPATH_FSCACHE_PATH);
  $cache_lifetime = variable_get('cache_lifetime', 0);
  
  if ($cid == '*') {
    fastpath_fscache_purge_loop($table);
  }
  
  if ($wildcard) {
    // TODO: what about wildcards?  Won't work with md5's...
    return;
  }
  
  // No arguments. clear page cache.
  if (is_null($cid) && is_null($table) && !$wildcard) {
    fastpath_fscache_purge_loop('cache_page');
  }
  
  if (empty($cid)) {
    if ($cache_lifetime || variable_get('page_cache_fastpath', 0)) {
      // We store the time in the current user's $user->cache variable which
      // will be saved into the sessions table by sess_write(). We then
      // simulate that the cache was flushed for this user by not returning
      // cached data that was cached before the timestamp.
      $user->cache = time();

      $cache_flush = variable_get('cache_flush', 0);
      $cache_files_expired = variable_get('cache_files_expired', 0);

      if (variable_get('page_cache_fastpath', 0)) {
        if ($cache_files_expired == 0) {
          // updating cache_flush invalidates all data cached before this time
          variable_set('cache_flush', time());
          // updating cache_files_expired so cron can delete expired files
          variable_set('cache_files_expired', time());
        }

        if (time() > ($cache_flush + $cache_lifetime)) {
          variable_set('cache_flush', time());
        }
      }
    }
  }
  else {
    $file = cache_filename($cid, $table);
    if ($fp = fopen($file, 'w')) {
      // only delete the cache file once we obtain an exclusive lock to prevent
      // deleting a cache file that is currently being read.
      if (flock($fp, LOCK_EX)) {
        unlink($file);
      }
    }
  }
}

/**
 * Sanitize the cache ID for use as a filename when caching to the filesystem.
 *
 * @param $cid
 *   The unique ID of the cache data to store.
 */
function cache_filename($cid, $table = 'cache') {
  $fspath = variable_get('file_cache', FASTPATH_FSCACHE_PATH);
  // lazily create the subdirs for this table if needed.
  cache_filename_create($fspath, $cid, $table);
  
  $hash = md5($cid);
  return  "$fspath/$table/". $hash{0}. '/'. md5($cid);
}

// Assure that the subdirectory for $table exists, and letter/number subdirs. if not, create them.
// Static variable cache insures that this happens once per table per request. see http://drupal.org/node/82738
function cache_filename_create($fspath, $cid, $table) {
  if (!isset($subdirectories[$table])) {
    $subdirectory = "$fspath/$table/";
    if (!is_dir($subdirectory)) {
      if (!@mkdir($subdirectory)) {
        // t() is not available
        drupal_set_message(strtr('The directory %directory must be created by an administrator, along with 1 character subdirectories.', array('%directory' => $subdirectory)));
      }
      else {
        @chmod($subdirectory, 0775); // Necessary for non-webserver users.
        // create number/letter specific subdirs.
        $ivalfrom = ord("a");
        $ivalto = ord("f");
        for($i = $ivalfrom; $i <= $ivalto; $i++) {
          @mkdir($subdirectory. chr($i), 0777);
        }

        for($i = 0; $i<10; $i++) {
          mkdir($subdirectory. $i, 0777);
        }
      }
      $subdirectories[$table] = TRUE;
    }
  }
}

/**
 * Main callback from DRUPAL_BOOTSTRAP_EARLY_PAGE_CACHE phase
 */
function page_cache_fastpath() {
  global $base_root;
  $file_cache = variable_get('fastpath_fscache_path', FASTPATH_FSCACHE_PATH);
  if ($file_cache && is_dir($file_cache)) {
    $cache = cache_get($base_root . request_uri(), 'cache_page');
    if (!empty($cache)) {
      // display cached page and exit
      drupal_page_header();
      if (function_exists('gzencode')) {
        header('Content-Encoding: gzip');
      }
      print $cache->data;
      return TRUE;
    }
  }
  else {
    // Warning message that $file_cache isn't set. We don't have access to the
    // t() function at this point.
    //drupal_set_message(t('Cache error, failed to locate cache directory "%dir"', array('%dir' => $file_cache)), 'error');
  }
}
